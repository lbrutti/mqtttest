var mqtt = require('mqtt')
var mongodb = require('mongodb');

var mongodbClient = mongodb.MongoClient;
var mongodbURI = 'mongodb://admin:admin@localhost:27017/mqttTest'
var deviceRoot = 'demo/device/'
var collection, client;


mongodbClient.connect(mongodbURI, setupCollection);

function setupCollection(err, db) {
    if (err) throw err;
    collection = db.collection('test_mqtt');
    client = mqtt.connect({
        port: 1883,
        host: 'localhost',
        keepalive: 10000
    });
    client.on('connect', function() {
        client.subscribe(deviceRoot + '#');
        client.on('message', insertEvent);
    });

}

function insertEvent(topic, payload) {
    var buf = Buffer.from(payload, 'base64');
    var key = topic.replace(deviceRoot, '');
    collection.update({
            _id: key
        }, {
            $push: {
                events: {
                    event: {
                        value: buf.toString('ascii'),
                        when: new Date()
                    }
                }
            }
        }, {
            upsert: true
        },
        function(err, docs) {
            if (err) {
                console.log('Insert fail');
            } // Improve error handling
        }
    );
}
